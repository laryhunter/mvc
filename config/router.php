<?php

return [
	'/' => [
		'controller' => \App\Controller\IndexController::class,
		'action' => 'index',
		'params' => []
	],
	'/edit' => [
		'controller' => \App\Controller\IndexController::class,
		'action' => 'edit',
		'params' => []
	],
	'/auth/login' => [
		'controller' => \App\Controller\AuthController::class,
		'action' => 'login',
		'params' => []
	],
	'/auth/logout' => [
		'controller' => \App\Controller\AuthController::class,
		'action' => 'logout',
		'params' => []
	],
];