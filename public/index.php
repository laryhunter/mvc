<?php

define('CONFIG_PATH', __DIR__ . '/../config');

spl_autoload_register(function ($path) {
	$dir = substr($path, 0, strrpos($path, '\\'));
	$class = substr($path, strrpos($path, '\\') + 1);
	require_once __DIR__ . '/../' . strtolower(str_replace('\\', '/', $dir)) . '/' . $class . '.php';
});

use Core\Application;
use Core\Router\RegExp;


$app = new Application(new RegExp(require_once(__DIR__ . '/../config/router.php')));
$app->run();