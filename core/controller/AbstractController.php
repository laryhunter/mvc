<?php
namespace Core\Controller;

use Core\Request\Request;
use Core\Session\Session;

abstract class AbstractController
{
	
	protected $request;
	
	protected $session;
	
	public function __construct()
	{
		$this->request = Request::instance();
		$this->session = Session::instance();
	}
}