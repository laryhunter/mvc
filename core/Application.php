<?php

namespace Core;

use Core\View\HtmlView;

class Application
{
	/**
	 * 
	 * @var \Core\Router\Routable
	 */
	private $router;
	
	public function __construct(\Core\Router\Routable $router)
	{
		$this->router = $router;
	}
	
	public function run()
	{
		try {
			$route = $this->router->dispatch($_SERVER['REQUEST_URI']);
			$view = $route->execute();
		} catch (\Core\Router\NotFoundException $e) {
			$view = new HtmlView('/errors/404.php');
		} catch (\Core\Router\BadActionException $e) {
			$view = new HtmlView('/errors/400.php');
		}
		
		$view->render();
	}
}