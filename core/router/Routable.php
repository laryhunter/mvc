<?php

namespace Core\Router;

interface Routable
{
	/**
	 * 
	 * @param string $url
	 * @return Route
	 * @throws NotFoundException
	 */
	public function dispatch(string $url) : Route;
}