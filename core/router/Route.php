<?php

namespace Core\Router;

class Route
{
	private $controller;
	
	private $action;
	
	private $params = [];
	
	public function __construct(string $controller, string $action, array $params = [])
	{
		$this->controller = $controller;
		$this->action = $action;
		$this->params = $params;
	}
	
	public function execute()
	{
		$reflection = new \ReflectionMethod($this->controller, sprintf('%sAction', $this->action));
		
		try {
			return $reflection->invokeArgs(new $this->controller(), $this->params);
		} catch (\ReflectionExtension $e) {
			throw new BadActionException();
		}
	}
}