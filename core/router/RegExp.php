<?php

namespace Core\Router;

class RegExp implements Routable
{
	private $routes = [];
	
	public function __construct(array $routes)
	{
		$this->routes = $routes;
	}
	
	public function dispatch(string $url) : Route
	{
		foreach ($this->routes as $path => $route) {
			$match = preg_match(sprintf('~^%s$~', $path), $url, $matches);
			if($match) {
				return new Route($route['controller'], $route['action'], array_slice($matches, 1));
			}
		}
		
		throw new NotFoundException();
	}
}