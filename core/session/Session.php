<?php

namespace Core\Session;

class Session
{
	private static $instance = null;
	
	public static function instance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	private function __construct()
	{
		session_start();
	}
	
	public function get($name)
	{
		return $_SESSION[$name];
	}
	
	public function set($name, $value)
	{
		$_SESSION[$name] = $value;
	}
	
	public function unset($name)
	{
		unset($_SESSION[$name]);
	}
	
	public function has($name)
	{
		return key_exists($name, $_SESSION);
	}
}