<?php

namespace Core\View;

use Core\Util\Config;

class HtmlView implements Viewable
{
	private $layout = '';
	
	private $template = '';
	
	private $params = [];
	
	private $headers = [];
	
	public function __construct(string $template = '', array $params = [])
	{
		$this->setLayout(Config::instance()->get('view.html.layout'));
		$this->setTemplate($template);
		$this->params = $params;
		$this->headers = Config::instance()->get('view.html.headers');
	}
	
	public function setLayout(string $name)
	{
		$this->layout = Config::instance()->get('view.html.template_path') . $name;
	}
	
	public function setTemplate(string $name)
	{
		$this->template = Config::instance()->get('view.html.template_path') . $name;
	}
	
	public function setParam(string $name, $value)
	{
		$this->params[$name] = $value;
	}
	
	public function render()
	{
		ob_start();
		include $this->template;
		$content = ob_get_contents();
		ob_end_clean();
		
		foreach ($this->headers as $header) {
			header($header);
		}
		
		if(is_readable($this->layout)) {
			ob_start();
			include $this->layout;
			$content = ob_get_contents();
			ob_end_clean();
		}
		
		echo $content;
	}
}