<?php

namespace Core\View;

interface Viewable
{
	public function render();
}