<?php

namespace Core\View;

class RedirectView implements Viewable
{
	private $location;
	
	public function __construct($location)
	{
		$this->location = $location;
	}
	
	public function render()
	{
		header(sprintf('Location: %s', $this->location));
	}
}