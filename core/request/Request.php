<?php

namespace Core\Request;

class Request
{
	const GET = 'GET';
	
	const POST = 'POST';
	
	private $params = [];
	
	private static $instance = null;
	
	public static function instance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	private function __construct()
	{
		$this->params = array_merge($_GET, $_POST);
	}
	
	
	public function get(string $name, $default = '')
	{
		return $this->has($name) ? $this->params[$name] : $default;
	}
	
	public function has(string $name)
	{
		return key_exists($name, $this->params);
	}
	
	public function getRequestMethod()
	{
		return $_SERVER['REQUEST_METHOD'];
	}
	
	public function isPost()
	{
		return $this->getRequestMethod() == self::POST;
	}
}