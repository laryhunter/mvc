<?php

namespace Core\Util;

class Config
{
	private static $instance = null;
	
	private $configs = [];
	
	public static function instance(): Config
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	private function __construct()
	{
	}
	
	public function get(string $path)
	{
		$chunks = explode('.', $path);
		
		$name = array_shift($chunks);
		
		if(!key_exists($name, $this->configs)) {
			$this->load($name);
		}
		
		return $this->find($chunks, $this->configs[$name]);
	}
	
	private function load(string $name)
	{
		$file = CONFIG_PATH . '/' . $name . '.php';
		
		if(!is_readable($file)) {
			throw new \Exception('Config file not found in ' . $file);
		}
		
		$this->configs[$name] = require_once($file);
	}
	
	private function find(array $path, array $search)
	{
		$node = array_shift($path);
		
		if(!key_exists($node, $search)) {
			throw new \Exception('Not found value');
		}
		
		if(!$path) {
			return $search[$node];
		}
		
		return $this->find($path, $search[$node]);
	}
}