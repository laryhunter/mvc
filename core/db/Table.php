<?php

namespace Core\Db;

use Core\Db\Statement\Select;
use Core\Db\Statement\Insert;
use Core\Db\Statement\Update;

class Table
{
	private $conn;
	
	private $table;
	
	public function __construct(string $conn, string $table)
	{
		$this->conn = Connection::instance($conn);
		$this->table = $table;
	}
	
	public function select()
	{
		return new Query(new Select($this->table), $this->conn);
	}
	
	public function insert($data)
	{
		return new Query(new Insert($this->table, array_keys($data), array_values($data)), $this->conn);
	}
	
	public function update($data)
	{
		return new Query(new Update($this->table, array_keys($data), array_values($data)), $this->conn);
	}
}