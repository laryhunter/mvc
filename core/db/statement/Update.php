<?php
namespace Core\Db\Statement;

class Update implements Statement
{
	private $table;
	
	private $fields;
	
	private $values;
	
	public function __construct(string $table, array $fields, array $values)
	{
		$this->table = $table;
		$this->fields = $fields;
		$this->values = $values;
	}
	
	public function prepare() : string
	{
		$set = [];
		foreach ($this->fields as $key => $field) {
			$set[] = sprintf("%s='%s'", $field, $this->values[$key]);
		}
		return sprintf('UPDATE %s SET %s', $this->table, implode(',', $set));
	}
}