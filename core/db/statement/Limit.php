<?php

namespace Core\Db\Statement;

class Limit implements Statement
{
	private $limit;
	
	private $offset;
	
	public function __construct(int $limit, int $offset = 0)
	{
		$this->limit = $limit;
		
		$this->offset = $offset;
	}
	
	public function prepare()
	{
		$sql = 'LIMIT ' . $this->limit;
		
		if($this->offset) {
			$sql .= ',' . $this->offset;
		}
		
		return $sql;
	}
}