<?php

namespace Core\Db\Statement;

class Select implements Statement
{
	private $table;
	
	public function __construct(string $table)
	{
		$this->table = $table;
	}
	
	public function prepare() : string
	{
		return sprintf('SELECT * FROM %s', $this->table);
	}
}