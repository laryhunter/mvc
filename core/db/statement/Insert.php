<?php

namespace Core\Db\Statement;

class Insert implements Statement
{
	private $table;
	
	private $fields;
	
	private $values;
	
	public function __construct(string $table, array $fields, array $values)
	{
		$this->table = $table;
		$this->fields = $fields;
		$this->values = $values;
	}
	
	public function prepare() : string
	{
		$values = [];
		foreach ($this->values as $value) {
			$values[] = sprintf("'%s'", $value);
		}
		return sprintf('INSERT INTO %s (%s) VALUES (%s)', $this->table, implode(',', $this->fields), implode(',', $values));
	}
}