<?php

namespace Core\Db\Statement;

class Where implements Statement
{
	
	private $field;
	private $condition;
	private $value;
	
	public function __construct(string $field, string $condition, string $value = '')
	{
		$this->field = $field;
		$this->condition = $condition;
		$this->value = $value;
	}
	
	public function prepare()
	{
		if($this->value) {
			return sprintf('%s %s "%s"', $this->field, $this->condition, $this->value);
		}
		
		return sprintf('%s %s', $this->field, $this->condition);
	}
}