<?php
namespace Core\Db;

class Query
{
	private $driver;
	
	private $query;
	
	private $wheres = [];
	
	private $whereType = 'AND';
	
	private $limit = null;
	
	public function __construct(\Core\Db\Statement\Statement $query, \Core\Db\Adapter\Adapter $driver)
	{
		$this->query = $query;
		$this->driver = $driver;
	}
	
	
	public function where(array $wheres, $type = 'AND')
	{
		$this->wheres = $wheres;
		$this->whereType = $type;
	}
	
	public function limit(\Core\Db\Statement\Limit $limit) {
		$this->limit = $limit;
	}
	
	public function toSql()
	{
		$sql = $this->query->prepare();
		
		if($this->wheres) {
			$wheres = [];
			foreach ($this->wheres as $where) {
				$wheres[] = $where->prepare();
			}
			$sql .= ' WHERE ' . implode(sprintf(' %s ', $this->whereType), $wheres);
		}
		
		if($this->limit) {
			$sql .= ' ' . $this->limit->prepare();
		}
		
		return $sql;
	}
	
	public function execute()
	{
		return $this->driver->execute($this->toSql());
	}
}