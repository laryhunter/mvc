<?php

namespace Core\Db\Adapter;

abstract class Adapter
{
	abstract public function execute(string $statement);
}