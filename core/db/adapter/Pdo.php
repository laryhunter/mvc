<?php

namespace Core\Db\Adapter;

class Pdo extends Adapter
{
	private $driver;
	
	public function __construct($dsn, $username, $password, $options)
	{
		$this->driver = new \PDO($dsn, $username, $password, $options);
	}
	
	public function execute(string $statement)
	{
		return $this->driver->query($statement)->fetchAll(\PDO::FETCH_ASSOC);
	}
}