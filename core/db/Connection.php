<?php
namespace Core\Db;

class Connection
{
	private static $adapters = [];
	
	private function __construct()
	{
	}
	
	public static function instance(string $name)
	{
		if(!key_exists($name, self::$adapters)) {
			self::$adapters[$name] = self::create(\Core\Util\Config::instance()->get('db.' . $name));
		}
		
		return self::$adapters[$name];
	}
	
	private static function create($config)
	{
		switch ($config['driver']) {
			case 'Pdo':
				return new \Core\Db\Adapter\Pdo($config['dsn'], $config['username'], $config['password'], $config['options']);
		}
		
		throw new \Exception('Driver not found');
	}
}