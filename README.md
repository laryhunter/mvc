# mvc

1. ��������� ����������� � ��
2. ������� ���� template_path �� �������� �� config/view.php

CREATE TABLE `field` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `field` VALUES ('1', '�������');
INSERT INTO `field` VALUES ('2', '���');
INSERT INTO `field` VALUES ('3', '��������');
INSERT INTO `field` VALUES ('4', '���� ��������');

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `field_id` int(11) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user` (`user_id`),
  KEY `fk_field` (`field_id`),
  CONSTRAINT `fk_field` FOREIGN KEY (`field_id`) REFERENCES `field` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `user` VALUES ('1', 'test', '1');
INSERT INTO `user` VALUES ('2', 'test1', '2');

CREATE VIEW `vw_profile` AS select `mvc`.`profile`.`id` AS `id`,`mvc`.`profile`.`user_id` AS `user_id`,`field`.`fid` AS `field_id`,`mvc`.`profile`.`value` AS `value`,`field`.`label` AS `label` from (((select `mvc`.`user`.`id` AS `uid`,`mvc`.`field`.`id` AS `fid`,`mvc`.`field`.`label` AS `label` from (`mvc`.`user` join `mvc`.`field`))) `field` left join `mvc`.`profile` on(((`mvc`.`profile`.`user_id` = `field`.`uid`) and (`mvc`.`profile`.`field_id` = `field`.`fid`)))) ;