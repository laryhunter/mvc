<?php

namespace App\Classes;

use App\Table\Field;
use App\Table\VwProfile;
use App\Table\Profile as ProfileTable;

class Profile
{
	private $user;
	
	private $fields = [];
	
	private $profile = [];
	
	public function __construct($user)
	{
		$this->user = $user;
		
		$fieldTable = new Field();
		$this->fields = $fieldTable->fetchAll();
		
		$profileTable = new VwProfile();
		
		$profile = $profileTable->fetch($user);
		
		foreach ($profile as $item) {
			$this->profile[$item['field_id']] = $item;
		}
	}
	
	public function save($fields)
	{
		$profileTable = new ProfileTable();
		foreach ($fields as $key => $value) {
			if($this->profile[$key]['id']) {
				$profileTable->save($this->user['id'], $key, $value);
			} else {
				$profileTable->create($this->user['id'], $key, $value);
			}
		}
	}
}