<?php
namespace App\Classes;

use Core\Session\Session;
use App\Table\User;

class Auth
{
	private static $instance = null;
	
	private $session;
	
	public static function instance()
	{
		if(self::$instance === null) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
	
	private function __construct()
	{
		$this->session = Session::instance();
	}
	
	public function isAuth()
	{
		return $this->session->has('uid');
	}
	
	public function user()
	{
		if(!$this->isAuth()) {
			throw new \Exception('Unauth');
		}
		
		$table = new User();
		return $table->fetchById($this->session->get('uid'));		
	}
	
	public function login($login, $password)
	{
		$table = new User();
		
		try {
			$user = $table->fetchByLoginAndPassword($login, $password);
		} catch (\Exception $e) {
			return false;
		}
		
		$this->session->set('uid', $user['id']);
		
		return true;
	}
	
	public function logout()
	{
		$this->session->unset('uid');
	}
}