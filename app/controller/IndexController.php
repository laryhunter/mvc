<?php

namespace App\Controller;

use App\Classes\Auth;
use Core\View\RedirectView;
use Core\View\HtmlView;
use App\Table\VwProfile;
use App\Classes\Profile;

class IndexController extends \Core\Controller\AbstractController
{
	public function indexAction() : \Core\View\Viewable
	{
		if(!Auth::instance()->isAuth()) {
			return new RedirectView('/auth/login');
		}
		$profile = new VwProfile();
		
		return new HtmlView('/index/index.php', [
			'profile' => $profile->fetch(Auth::instance()->user())
		]);
	}
	
	public function editAction()
	{
		if(!Auth::instance()->isAuth()) {
			return new RedirectView('/auth/login');
		}
		
		if($this->request->isPost()) {
			
			$model = new Profile(Auth::instance()->user());
			$model->save($this->request->get('field'));
			
			return new RedirectView('/');
		}
		
		$profile = new VwProfile();
		
		return new HtmlView('/index/edit.php', [
			'profile' => $profile->fetch(Auth::instance()->user())
		]);
	}
}