<?php

namespace App\Controller;

use Core\View\HtmlView;
use App\Classes\Auth;
use Core\View\RedirectView;

class AuthController extends \Core\Controller\AbstractController
{
	public function loginAction()
	{
		if(Auth::instance()->isAuth()) {
			return new RedirectView('/');
		}
		
		if($this->request->isPost()) {
			
			$auth = Auth::instance()->login($this->request->get('login'), $this->request->get('password'));
			
			if($auth) {
				return new RedirectView('/');
			}
		}
		
		return new HtmlView('/auth/login.php');
	}
	
	public function logoutAction()
	{
		Auth::instance()->logout();
		return new RedirectView('/auth/login');
	}
}