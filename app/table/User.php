<?php

namespace App\Table;

use Core\Db\Table;
use Core\Db\Statement\Where;
use Core\Db\Statement\Limit;

class User extends Table
{
	public function __construct()
	{
		parent::__construct('db', 'user');
	}
	
	public function fetchByLoginAndPassword(string $login, string $password)
	{
		$select = $this->select();
		$select->where([
			new Where('login', '=', $login),
			new Where('password', '=', $password)
		]);
		$select->limit(new Limit(1));
		
		$data = $select->execute();
		
		if($data) {
			return array_shift($data);
		}
		
		throw new \Exception('Not found');
	}
	
	public function fetchById(int $id)
	{
		$select = $this->select();
		$select->where([
			new Where('id', '=', $id)
		]);
		$select->limit(new Limit(1));
		$data = $select->execute();
		
		if($data) {
			return array_shift($data);
		}
		
		throw new \Exception('Not found');
	}
}