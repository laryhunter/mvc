<?php

namespace App\Table;

use Core\Db\Table;
use Core\Db\Statement\Where;

class VwProfile extends Table
{
	public function __construct()
	{
		parent::__construct('db', 'vw_profile');
	}
	
	public function fetch($user)
	{
		$select = $this->select();
		$select->where([
			new Where('user_id', '=', $user['id']),
			new Where('user_id', 'IS NULL'),
		], 'OR');
		
		return $select->execute();
	}
}