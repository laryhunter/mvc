<?php
namespace App\Table;

use Core\Db\Table;
use Core\Db\Statement\Where;

class Profile extends Table
{
	public function __construct()
	{
		parent::__construct('db', 'profile');
	}
	
	public function create($userId, $fieldId, $value)
	{
		$this->insert([
			'user_id' => $userId,
			'field_id' => $fieldId,
			'value' => $value
		])->execute();
	}
	
	public function save($userId, $fieldId, $value)
	{
		$update = $this->update([
				'value' => $value
		]);
		$update->where([
			new Where('user_id', '=', $userId),
				new Where('field_id', '=', $fieldId),
		]);
		$update->execute();
	}
}