<?php

namespace App\Table;

use Core\Db\Table;

class Field extends Table
{
	public function __construct()
	{
		parent::__construct('db', 'field');
	}
	
	public function fetchAll()
	{
		return $this->select()->execute();
	}
}